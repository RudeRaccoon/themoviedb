﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Web.Syndication;
using TheMovieDB.Models;
using Windows.UI.Popups;

// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace TheMovieDB
{
    /// <summary>
    /// Eine leere Seite, die eigenständig verwendet werden kann oder auf die innerhalb eines Rahmens navigiert werden kann.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            //Wird benötigt um den Seiteninhalt zu speichern
            this.NavigationCacheMode = Windows.UI.Xaml.Navigation.NavigationCacheMode.Enabled;
        }

        private void GridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            //Übergibt das Model Movie zur Detailseite
            Movie model = (Movie)e.ClickedItem;
            Frame frame = (Frame)Window.Current.Content;
            frame.Navigate(typeof(DetailPage), model);
        }
        
        private async void SearchMovie(SearchBox sender, SearchBoxQuerySubmittedEventArgs args)
        {
            if (String.IsNullOrEmpty(args.QueryText))
            {
                var messageDialog = new MessageDialog("Für eine Suche muss auch ein Suchbegriff eingegeben werden.");
                await messageDialog.ShowAsync();
            }
            else
            {
                await (DataContext as ViewModel).LoadSearchedMovies(args);
            }
            
        }
        private async void RequestTop(object sender, RoutedEventArgs e)
        {
            await(DataContext as ViewModel).LoadTopRatedMovies();
        }

        private async void Popular(object sender, RoutedEventArgs e)
        {
            await(DataContext as ViewModel).LoadPopularMovies();
        }

        private async void NowPlaying(object sender, RoutedEventArgs e)
        {
            await(DataContext as ViewModel).LoadPlayingNowMovies();
        }
        
    }
}
