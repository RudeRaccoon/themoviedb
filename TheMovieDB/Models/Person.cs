﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading.Tasks;

namespace TheMovieDB.Models
{
    //Model als Basis für Personen, beinhaltet alle benötigten Informationen
    public class Person
    {
        public string Name { get; set; }
        public string KnowsAs { get; set; }
        public string Birthday { get; set; }
        public string Deathday { get; set; }
        public string Homepage { get; set; }
        public string Poster { get; set; }
    }
}
