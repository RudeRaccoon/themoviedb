﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading.Tasks;

namespace TheMovieDB.Models
{
    //Model als Basis für die Filme, beinhaltet nur die benötigten Informationen
    class Movie
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string OriginalTitle { get; set; }
        public string TagLine { get; set; }
        public string OverView { get; set; }
        public string PosterPath { get; set; }
        public string BackdropPath { get; set; }
        public bool Adult { get; set; }
        public string Budget { get; set; }
        public string Homepage { get; set; }
        public string ReleaseDate { get; set; }
        public decimal Popularity { get; set; }
        public string Rating { get; set; }
        public int Votes { get; set; }
        public int? Runtime {get; set;}
        public MediaCredits Credits { get; set; }


    }
}
