﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TheMovieDB.Models
{
    class ViewModelCasts : System.ComponentModel.INotifyPropertyChanged
    {
        public ObservableCollection<Person> Casts { get; set; }
        public ViewModelCasts(Movie movie)
        {
            Casts = new ObservableCollection<Person>();
            LoadCast(movie);
        }
        public async Task LoadCast(Movie movie)
        {
            var cTokenSource = new CancellationTokenSource();
            // Create a cancellation token from CancellationTokenSource
            var cToken = cTokenSource.Token;
            using (var client = new System.Net.TMDb.ServiceClient("cca5596bdf8ea2f4b621ff76590fbed1"))
            {
                var personIDs = movie.Credits.Cast.Select(s => s.Id).Union(movie.Credits.Crew.Select(s => s.Id));
                foreach (var id in personIDs)
                {
                    var person = await client.People.GetAsync(id, true, cToken);
                    string poster;
                    if (String.IsNullOrEmpty(person.Poster))
                    {
                        poster = "Assets/poster.png";
                    }
                    else
                    {
                        poster = "http://image.tmdb.org/t/p/w185" + person.Poster;
                    }
                    Casts.Add(new Person()
                    {
                        Name = person.Name,
                        Birthday = person.BirthDay,
                        Deathday = person.DeathDay,
                        Homepage = person.HomePage,
                        Poster = poster
                    });
                }
            }
        }


        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
    }
}
