﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.TMDb;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Xaml.Controls;
using Windows.Web.Http;
using Windows.Web.Syndication;

namespace TheMovieDB.Models
{
    class ViewModel : System.ComponentModel.INotifyPropertyChanged
    {
        public ObservableCollection<Movie> Items { get; private set; }
        private bool isLoading;
        public bool IsLoading
        {
            get
            {
                return isLoading;
            }
            set
            {
                isLoading = value;
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs("IsLoading"));
            }
        }
        public ViewModel()
        {
            Items = new ObservableCollection<Movie>();
        }
        //Erstellen der Filmliste mit momentan gut Bewerteten Filmem von TheMovieDB.org
        public async Task LoadTopRatedMovies()
        {
            IsLoading = true;
            // Create a cancellation token from CancellationTokenSource
            var cTokenSource = new CancellationTokenSource();
            var cToken = cTokenSource.Token;
            using (var client = new ServiceClient("cca5596bdf8ea2f4b621ff76590fbed1"))
            {
                Movies movies = await client.Movies.GetTopRatedAsync("DE", 1, cToken);
                Load(movies, cToken);
            }
            IsLoading = false;    
        }
        //Erstellen der Filmliste mit Filmen die als Suchergebniss zurückkommen
        public async Task LoadSearchedMovies(SearchBoxQuerySubmittedEventArgs args)
        {
            IsLoading = true;
            var cTokenSource = new CancellationTokenSource();
            // Create a cancellation token from CancellationTokenSource
            var cToken = cTokenSource.Token;
            using (var client = new ServiceClient("cca5596bdf8ea2f4b621ff76590fbed1"))
            {
                Movies movies = await client.Movies.SearchAsync(args.QueryText, "DE", true, 1, cToken);
                Load(movies, cToken);
            }
            IsLoading = false;
        }
        //Erstellen der Filmliste mit momentan häufig gesuchten Filmen von TheMovieDB.org
        public async Task LoadPopularMovies()
        {
            IsLoading = true;
            var cTokenSource = new CancellationTokenSource();
            // Create a cancellation token from CancellationTokenSource
            var cToken = cTokenSource.Token;
            using (var client = new ServiceClient("cca5596bdf8ea2f4b621ff76590fbed1"))
            {
                Movies movies = await client.Movies.GetPopularAsync("DE", 1, cToken);
                Load(movies, cToken);
            }
            IsLoading = false;
        }
        //Erstellen der Filmliste mit momentan neuen Filmen
        public async Task LoadPlayingNowMovies()
        {
            IsLoading = true;
            var cTokenSource = new CancellationTokenSource();
            // Create a cancellation token from CancellationTokenSource
            var cToken = cTokenSource.Token;
            using (var client = new ServiceClient("cca5596bdf8ea2f4b621ff76590fbed1"))
            {
                Movies movies = await client.Movies.GetNowPlayingAsync("DE", 1, cToken);
                Load(movies, cToken);
            }
            IsLoading = false;
        }
        //Ruft Informationen über jeden Film in den vorher erstellen Filmliste ab und speichert diese in der ObservableCollection<Movie> Items die als DataContext dient
        private async void Load(Movies movies, CancellationToken cToken)
        {
            Items.Clear();
            using (var client = new ServiceClient("cca5596bdf8ea2f4b621ff76590fbed1"))
            {
                foreach (var item in movies.Results)
                {
                    var movie = await client.Movies.GetAsync(item.Id, "DE", true, cToken);
                    string poster;
                    if (String.IsNullOrEmpty(movie.Poster))
                    {
                        poster = "Assets/poster.png";
                    }
                    else
                    {
                        poster = "http://image.tmdb.org/t/p/w500" + movie.Poster;
                    }
                    Items.Add(new Movie()
                    {
                        Id = movie.Id,
                        Title = movie.Title,
                        OriginalTitle = movie.OriginalTitle,
                        TagLine = movie.TagLine,
                        OverView = movie.Overview,
                        PosterPath = poster,
                        BackdropPath = "https://image.tmdb.org/t/p/w780" + movie.Backdrop,
                        Adult = movie.Adult,
                        Budget = movie.Budget.ToString("C"),
                        ReleaseDate = String.Format("{0:d/M/yyyy}", movie.ReleaseDate),
                        Homepage = movie.HomePage,
                        Popularity = movie.Popularity,
                        Rating = movie.VoteAverage + " von 10",
                        Votes = movie.VoteCount,
                        Runtime = movie.Runtime,
                        Credits = movie.Credits
                    });       
                }
            }     
        }
        public event PropertyChangedEventHandler PropertyChanged;
    }
}