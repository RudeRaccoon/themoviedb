﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using TheMovieDB.Models;
using System.Threading;
using System.Collections.ObjectModel;
using System.ComponentModel;



// Die Elementvorlage "Leere Seite" ist unter http://go.microsoft.com/fwlink/?LinkId=234238 dokumentiert.

namespace TheMovieDB
{
    public sealed partial class DetailPage : Page
    {
        
        
        public DetailPage()
        {
            this.InitializeComponent();
            
            
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //Übergebenen Film als DataContext zuweisen
            DataContext = e.Parameter;
            //Initialisieren und Zuweisen des DataContext für das GridView zum Schauspieler anzeigen
            ViewModelCasts casts = new ViewModelCasts((Movie)e.Parameter);
            CastView.DataContext = casts;

        }
        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            (Window.Current.Content as Frame).GoBack();
        }
    }
}
